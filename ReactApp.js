class Game extends React.Component{
    render() {
      return (
      <React.Fragment> 
      <h1 id ="arena">{this.props.arena}</h1>
      <div id= "mavs">
        <Team
        name={this.props.mavs}
        logo={this.props.mavsLogo}
        />
      </div>
      <div id= "cavs">
        <Team
        name={this.props.cavs}
        logo={this.props.cavsLogo}
        />
      </div>
      </React.Fragment>
      )
    }
  }
  
  
  
  
  // Deafault App component that all other compents are rendered through
  function App(props){
    return (
      <React.Fragment>
        < Game
         arena="American Airlines Center"
         mavs="Mavericks" 
         cavs="Cavaliers" 
         mavsLogo="mavs.jpg" 
         cavsLogo="cavs.png"/>
      </React.Fragment>
     
    )
  }
  
  //Render the application
  ReactDOM.render(
    <App />,
    document.getElementById('root')
  );
  
  